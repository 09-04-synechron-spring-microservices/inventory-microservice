package com.classpath.inventorymicroservice.model;

public enum OrderStatus {
    ORDER_ACCPETED,
    ORDER_PENDING,
    ORDER_CANCELLED,
    ORDER_FULFILLED
}
