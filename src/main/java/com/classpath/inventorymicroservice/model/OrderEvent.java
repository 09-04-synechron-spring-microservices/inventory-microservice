package com.classpath.inventorymicroservice.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class OrderEvent {
    private Order order;
    private OrderStatus orderStatus;
    private LocalDateTime orderTimeStamp;
}
