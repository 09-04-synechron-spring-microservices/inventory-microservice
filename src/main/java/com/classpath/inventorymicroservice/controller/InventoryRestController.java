package com.classpath.inventorymicroservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
@Slf4j
public class InventoryRestController {

    private int counter = 1000;

    @PostMapping
    public int updateQty(){
        log.info("Inside the updatedQty method :: {}", counter);
        return --counter;
    }
    @GetMapping
    public int getCounter(){
        return counter;
    }
}
